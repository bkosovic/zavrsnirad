#include <RH_ASK.h>
#ifdef RH_HAVE_HARDWARE_SPI
#include <SPI.h>
#endif

// Dodjeljivanje GPIO pina za LED diodu te definicija potrebne varijable.
#define selectorLED 14
int selectorLedState = 0;

// Inicijalizacija drivera, za predajnik se koristi pin 5.
RH_ASK driver(2000, 4, 5, 0); 

// U sljedećem bloku su dodjeljeni GPIO pinovi za tastere te su definirane potrebne varijable.
const int buttonPinA = 37;
int buttonStateA = LOW;
int lastButtonStateA = LOW;

const int buttonPinB = 39;
int buttonStateB = LOW;
int lastButtonStateB = LOW;

const int buttonPinC = 34;
int buttonStateC = LOW;
int lastButtonStateC = LOW;

const int buttonPinD = 32;
int buttonStateD = LOW;
int lastButtonStateD = LOW;

// Funkcija koja se poziva pri pokretanju mikrokontrolera,
// inicijalizira potrebne GPIO pinove te driver za RF modul.
void setup(){
  pinMode(selectorLED, OUTPUT);
  digitalWrite(selectorLED, LOW);
  digitalWrite(selectorLED, HIGH);
  delay(500);
  digitalWrite(selectorLED, LOW);
#ifdef RH_HAVE_SERIAL
  Serial.begin(9600); // Debugging only
#endif
  pinMode(buttonPinA, INPUT_PULLDOWN);
  pinMode(buttonPinB, INPUT_PULLDOWN);
  pinMode(buttonPinC, INPUT_PULLDOWN);
  pinMode(buttonPinD, INPUT_PULLDOWN);
  if (!driver.init()){
#ifdef RH_HAVE_SERIAL
    Serial.println("init failed");
#else
    ;
#endif
  }
}

// Funkcija koja se beskonačno izvodi, tj. dok se ne ugasi mikrokontroler
void loop(){
  bool readyToSend = false;
  char msg;

  // Učitavanje trenutnog stanja na tasterima
  buttonStateA = digitalRead(buttonPinA);
  buttonStateB = digitalRead(buttonPinB);
  buttonStateC = digitalRead(buttonPinC);
  buttonStateD = digitalRead(buttonPinD);

  // Odabir prijemnika na koji se šalju naredbe
  if (buttonStateA == HIGH && lastButtonStateA == LOW){
    lastButtonStateA = HIGH;
    if (selectorLedState == 0){
      selectorLedState = 1;
      digitalWrite(selectorLED, HIGH);
    }
    else if (selectorLedState == 1){
      selectorLedState = 0;
      digitalWrite(selectorLED, LOW);
    }
    delay(700);
  }
  // Sljedeći blok se koristi u svrhu debouncinga.
  else{
    lastButtonStateA = LOW;
  }
  // učitavanje odgovarajuće vrijednosti za slanje u varijablu msg
  if (buttonStateB == HIGH && lastButtonStateB == LOW){
    lastButtonStateB = HIGH;
    readyToSend = true;
    if (selectorLedState == 0){
      delay(200);
      msg = 'b';
    }
    else if (selectorLedState == 1){
      delay(200);
      msg = 'x';
    }
  }
  else if (buttonStateC == HIGH && lastButtonStateC == LOW){
    lastButtonStateC = HIGH;
    readyToSend = true;
    if (selectorLedState == 0){
      delay(200);
      msg = 'c';
    }
    else if (selectorLedState == 1){
      delay(200);
      msg = 'y';
    }
  }
  else if (buttonStateD == HIGH && lastButtonStateD == LOW){
    lastButtonStateD = HIGH;
    readyToSend = true;
    if (selectorLedState == 0){
      delay(200);
      msg = 'd';
    }
    else if (selectorLedState == 1){
      delay(200);
      msg = 'z';
    }
  }
  // Sljedeći blok se koristi u svrhu debouncinga.
  else{
    lastButtonStateB = LOW;
    lastButtonStateC = LOW;
    lastButtonStateD = LOW;
  }

  // Slanje vrijednosti spremljene u varijabli msg.
  if (readyToSend == true){
    Serial.println(msg);
    const char *send = &msg;
    driver.send((uint8_t *)send, strlen(send));
    driver.waitPacketSent();
    driver.send((uint8_t *)send, strlen(send));
    driver.waitPacketSent();
    Serial.println("poslano");
    readyToSend = false;
    delay(500);
  }
}
